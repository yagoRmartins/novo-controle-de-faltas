<?php
     @session_start();
?>
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="visualizacao.php">
                                <span data-feather="home"></span>
                                Inicio <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php
                           
                            if ($_SESSION['tipificacao'] == 0){
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="usuarios.php">
                                        <span data-feather="file"></span>
                                        Usuarios
                                    </a>
                                </li>
                                <?php
                            }
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <span data-feather="users"></span>
                                Chamada
                            </a>
                        </li>
                        <?php

                            if ($_SESSION['tipificacao'] == 0){
                            ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="turma.php">
                                        <span data-feather="bar-chart-2"></span>
                                        Turma
                                    </a>
                                </li>
                                <?php
                            }
                        ?>

                    </ul>

                    
                </div>
            </nav>