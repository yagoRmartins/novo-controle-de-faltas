

<?php
    session_start();
    if(!isset($_SESSION['usuario']) && !isset($_SESSION['senha'])){
        header("Location: index.php");
        exit;
    }
    ?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Sistema CF</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="importacoes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="importacoes/dashboard.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Sistema CF</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="index.php">Sair</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
           <?php
            include_once('menu.php');
           ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div>

                <div class="container">
    <form action="func_cadastrar.php" method="post" class="form-login">
      <h2 class="form-login-heading">Cadastro de Administrador</h2>

        <div class="form-group">  
            <label for="inputName">Nome: </label>
            <input type="text" class="form-control" name="nomeCadastro" id="inputNome" placeholder="Nome" required autofocus>
        </div>
        <div class="form-group">  
            <label for="inputUser">Usuario: </label>
            <input type="text" class="form-control" name="usuarioCadastro" id="inputUsuario" placeholder="Usuario" maxlength="30" required>
        </div>
        <div class="form-group">
            <label for="inputPassword">Senha: </label>
            <input type="password" class="form-control" name="senhaCadastro" id="inputPassword" placeholder="Senha" maxlength="15" required>
        </div>

        <div>
            <input type = "hidden" value = "0" name = "tipificacao"/>
        </div><br>
            <div class="row">
                <div class = "col-md-3">
                    <button type="submit" class="btn btn-success btn-block">Cadastrar</button>
                </div>
            </div>
    </form>
    <p class="text-center text-danger">
        <?php
             if (isset($_SESSION['cadastroErro'])){
                echo $_SESSION['cadastroErro'];
                unset ($_SESSION['cadastroErro']);
            }
        ?>
        </p>
  </div>

              </div>



            </main>


        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="importacoes/popper.min.js"></script>
    <script src="importacoes/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

</body>

</html>