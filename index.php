<?php
    session_start();
    
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Controle de faltas</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <script src='main.js'></script>
</head>
<body>

  <div class="container">
    <form action="logar.php" method="post" class="form-login">
      <h2 class="form-login-heading">Login</h2>

        <div class="form-group">  
            <label for="inputUser">Usuario: </label>
            <input type="text" class="form-control" name="usuario" id="inputUsuario" placeholder="Usuario" maxlength="30" required autofocus>
        </div>

        <div class="form-group">
            <label for="inputPassword">Senha: </label>
            <input type="password" class="form-control" name="senha" id="inputPassword" placeholder="Senha" maxlength="15" required><br/>
        </div>
      
        <button type="submit" class="btn btn-primary btn-block">Entrar</button>
    </form>

        <p class = "text-center text-danger">
        <?php
            if (isset($_SESSION['loginErro'])){
                echo $_SESSION['loginErro'];
                unset ($_SESSION['loginErro']);
            }
        ?>
        </p>
  </div>

    <script type="text/javascript" src="bootstrap/js/jquery.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>