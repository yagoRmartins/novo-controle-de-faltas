<?php
    session_start();
    include_once("conexao.php");
?>

<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Sistema CF</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="importacoes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="importacoes/dashboard.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Sistema CF</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="index.php">Sair</a>
            </li>
        </ul>
    </nav>

    <?php
    
    require ('conexao.php');
     
        $result_usuarios = "SELECT * FROM login_session WHERE tipificacao = 0";
        $resultado_usuarios = mysqli_query($conn, $result_usuarios);
       
        if($resultado_usuarios){
        } else {
            echo "Error: " .$sql."<br>".mysqli_error($conn);
        }


        
    ?>

        <div class="container_fluid">
            <div class="row">
                
                <?php
                include_once('menu.php');
                ?>
                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <div>

                <a type="button" class="btn btn-success" href="cadastro_adm.php">Cadastrar administrador</a>
                                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Usuario</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                        <td><?php
                            while ($row = mysqli_fetch_array($resultado_usuarios))
                            {
                                ?>
                                    <tr>
                                        <td>    
                                            <?php
                                            print $row['id_login'];  
                                            ?> 
                                        </td>
                                        <td>
                                            <?php
                                            print $row['nome'];  
                                            ?> 
                                        </td>
                                        <td>
                                            <?php
                                            print $row['usuario'];  
                                            ?> 
                                        </td>
                                        <td>
                                        <a href = "editar.php?id_url_login=<?php echo $row['id_login'] ?>" class="btn btn-primary">Editar informações</a>
                                        </td>
                                    </tr>

                            <?php
                            }
                            ?>
                        </tbody>
                    </table>

              </div>



            </main>
                
                       
            </div>
        </div>
</body>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="importacoes/popper.min.js"></script>
    <script src="importacoes/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
    feather.replace()
    </script>

    <!-- Graphs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
</head>
</html>